<?php

namespace Drupal\panopoly_migrate\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\panopoly_migrate\Event\MigrateFormattedTextEvent;
use Drupal\panopoly_migrate\PanopolyMigrateEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Converts formatted text fields.
 *
 * @MigrateProcessPlugin(
 *   id = "panopoly_migrate_formatted_text",
 *   handle_multiples = TRUE
 * )
 */
class FormattedText extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The event dispatcher.
   *
   * @var Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a FormattedText object.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $unpack = FALSE;
    if (isset($value['value'])) {
      $value = [$value];
      $unpack = TRUE;
    }

    foreach ($value as &$formatted_text) {
      $event = new MigrateFormattedTextEvent($formatted_text['value'], $formatted_text['summary'] ?? '', $formatted_text['format']);
      $this->eventDispatcher->dispatch($event, PanopolyMigrateEvents::MIGRATE_FORMATTED_TEXT);
      $formatted_text['value'] = $event->getValue();
      $formatted_text['summary'] = $event->getSummary();
      $formatted_text['format'] = $event->getFormat();
    }

    if ($unpack) {
      $value = $value[0];
    }

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return TRUE;
  }

}
