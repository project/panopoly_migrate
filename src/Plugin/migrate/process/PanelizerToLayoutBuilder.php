<?php

namespace Drupal\panopoly_migrate\Plugin\migrate\process;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\panopoly_migrate\Event\MigrateLayoutEvent;
use Drupal\panopoly_migrate\Event\MigratePaneEvent;
use Drupal\panopoly_migrate\Event\MigratePanePlacementEvent;
use Drupal\panopoly_migrate\PanopolyMigrateEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Converts the Panelizer to Layout Builder.
 *
 * @MigrateProcessPlugin(
 *   id = "panelizer_to_layout_builder",
 *   handle_multiples = TRUE
 * )
 */
class PanelizerToLayoutBuilder extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The UUID generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidGenerator;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The layout manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutManager;

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * Constructs a PanelizerToLayoutBuilder.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_generator
   *   The UUID generator.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Layout\LayoutPluginManagerInterface $layout_manager
   *   The layout manager.
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UuidInterface $uuid_generator, EventDispatcherInterface $event_dispatcher, LayoutPluginManagerInterface $layout_manager, BlockManagerInterface $block_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->uuidGenerator = $uuid_generator;
    $this->eventDispatcher = $event_dispatcher;
    $this->layoutManager = $layout_manager;
    $this->blockManager = $block_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('uuid'),
      $container->get('event_dispatcher'),
      $container->get('plugin.manager.core.layout'),
      $container->get('plugin.manager.block')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\migrate\MigrateException
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $sections = [];

    foreach ($value as $panelizer_display) {
      // Only address "full page" displays.
      if (!in_array($panelizer_display['view_mode'], ['page_manager', 'default', 'full'])) {
        continue;
      }

      $layout = $panelizer_display['layout'];
      $layout_settings = $panelizer_display['layout_settings'];
      $this->migrateLayout($panelizer_display, $layout, $layout_settings);

      $components = [];
      foreach ($panelizer_display['panes'] as $pane) {
        $region = $this->migrateRegion($panelizer_display, $layout, $pane);
        if ($region !== NULL) {
          $component = $this->migratePane($panelizer_display, $layout, $region, $pane);
          if ($component) {
            $components[] = $component;
          }
        }
      }

      $sections[] = new Section($layout, $layout_settings, $components);

      // It appears as though Layout Builder can't have overrides for multiple
      // view modes? So, we only return one section for the first Panelizer
      // display that we encounter.
      break;
    }

    return $sections;
  }

  /**
   * Migrates D7 Panels layout to core layout.
   *
   * @param array $panelizer_display
   *   The Panelizer display.
   * @param string $layout
   *   The layout ID.
   * @param array $layout_settings
   *   The layout settings.
   *
   * @throws \Drupal\migrate\MigrateException
   *   When the resulting layout isn't valid.
   */
  public function migrateLayout(array $panelizer_display, string &$layout, array &$layout_settings) {
    $event = new MigrateLayoutEvent($panelizer_display, $layout, $layout_settings);
    $this->eventDispatcher->dispatch($event, PanopolyMigrateEvents::MIGRATE_LAYOUT);
    $layout = $event->getLayout();
    $layout_settings = $event->getSettings();

    // Attempt to initialize this layout with these settings.
    try {
      $this->layoutManager->createInstance($layout, $layout_settings);
    }
    catch (PluginException $e) {
      throw new MigrateException("Unable to migrate to layout {$layout}");
    }
  }

  /**
   * Migrates D7 Panels Pane placements.
   *
   * @param array $panelizer_display
   *   The Panelizer display.
   * @param string $layout
   *   The layout ID.
   * @param array $pane
   *   The pane data.
   *
   * @return string|null
   *   The region, or NULL if this pane should be skipped.
   *
   * @throws \Drupal\migrate\MigrateException
   *   When the resulting region isn't valid.
   */
  protected function migrateRegion(array $panelizer_display, string $layout, array $pane) {
    $event = new MigratePanePlacementEvent($panelizer_display, $layout, $pane);
    $this->eventDispatcher->dispatch($event, PanopolyMigrateEvents::MIGRATE_PANE_PLACEMENT);

    if ($event->isSkip()) {
      return NULL;
    }

    $region = $event->getRegion();

    if (!$this->checkLayoutRegion($layout, $region)) {
      throw new MigrateException("Unable to migrate to region {$region} on layout {$layout}");
    }

    return $region;
  }

  /**
   * Checks if a layout region is valid.
   *
   * @param string $layout
   *   The layout ID.
   * @param string $region
   *   The region.
   *
   * @return bool
   *   TRUE if the region is valid; otherwise FALSE.
   */
  protected function checkLayoutRegion(string $layout, string $region): bool {
    try {
      $layout_definition = $this->layoutManager->getDefinition($layout);
    }
    catch (PluginNotFoundException $e) {
      return FALSE;
    }

    if ($layout_definition === NULL) {
      return FALSE;
    }

    $layout_regions = $layout_definition->getRegions();
    return isset($layout_regions[$region]);
  }

  /**
   * Migrates a D7 Panels Pane to a layout_builder component.
   *
   * @param array $panelizer_display
   *   The Panelizer display.
   * @param string $layout
   *   The new layout.
   * @param string $region
   *   The new region.
   * @param array $pane
   *   The pane data.
   *
   * @return \Drupal\layout_builder\SectionComponent|null
   *   The component, or NULL if this pane should be skipped.
   *
   * @throws \Drupal\migrate\MigrateException
   *   When the pane can't be migrated.
   */
  protected function migratePane(array $panelizer_display, string $layout, string $region, array $pane) {
    $event = new MigratePaneEvent($panelizer_display, $layout, $region, $pane);
    $this->eventDispatcher->dispatch($event, PanopolyMigrateEvents::MIGRATE_PANE);

    if ($event->isSkip()) {
      return NULL;
    }

    $block_id = $event->getBlockId();
    if (empty($block_id) || !$this->blockManager->hasDefinition($block_id)) {
      /** @var Drupal\Core\Site\Settings $settings */
      $settings = \Drupal::service('settings');
      if ($settings->get('panopoly_migrate_skip_unknown_blocks', FALSE)) {
        return NULL;
      }

      $full_id = $pane['type'];
      if (!empty($pane['subtype'])) {
        $full_id .= ":{$pane['subtype']}";
      }
      $attempted = '';
      if (!empty($block_id)) {
        $attempted = " (attempted migration to {$block_id})";
      }
      throw new MigrateException("Unable to migrate pane {$full_id}{$attempted}");
    }

    $config = $event->getBlockConfiguration();
    $config['id'] = $block_id;

    $uuid = $pane['uuid'] ?? $this->uuidGenerator->generate();
    $component = new SectionComponent($uuid, $region, $config, [], $event->getThirdPartySettings());
    $component->setWeight($event->getWeight());
    return $component;
  }

  /**
   * Replaces panels keywords with tokens.
   *
   * @param string $value
   *   Value containing panels keywoards.
   *
   * @return string
   *   Value containing tokens.
   */
  protected function replacePanelsKeywords($value) {
    $value = (string) $value;
    $value = preg_replace('/%([^\s]+)/', '[\1]', $value);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return TRUE;
  }

}
