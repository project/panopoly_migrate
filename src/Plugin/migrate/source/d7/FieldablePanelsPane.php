<?php

namespace Drupal\panopoly_migrate\Plugin\migrate\source\d7;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\migrate\Annotation\MigrateSource;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal 7 fieldable panels pane source from database.
 *
 * @MigrateSource(
 *   id = "d7_fieldable_panels_pane",
 *   source_module = "fieldable_panels_panes"
 * )
 */
class FieldablePanelsPane extends FieldableEntity {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * The join options between the main and revision tables.
   */
  const JOIN = '[fpp].[fpid] = [fppr].[fpid]';

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Select node in its last revision.
    $query = $this->select('fieldable_panels_panes_revision', 'fppr')
      ->fields('fpp', [
        'fpid',
        'bundle',
        'link',
        'path',
        'reusable',
        'admin_title',
        'admin_description',
        'category',
        'view_access',
        'edit_access',
        'created',
        'changed',
        'uuid',
        'language',
      ])
      ->fields('fppr', [
        'vid',
        'timestamp',
        'uid',
        'title',
        'log',
        'vuuid',
      ]);
    $query->innerJoin('fieldable_panels_panes', 'fpp', static::JOIN);
    $query->orderBy('fppr.vid');

    if (isset($this->configuration['fpp_bundle'])) {
      $query->condition('fpp.bundle', (array) $this->configuration['fpp_bundle'], 'IN');
    }

    // Get any entity translation revision data.
    if ($this->getDatabase()->schema()
      ->tableExists('entity_translation_revision')) {
      $query->leftJoin('entity_translation_revision', 'etr', '[fppr].[fpid] = [etr].[entity_id] AND [fppr].[vid] = [etr].[revision_id]');
      $query->fields('etr', [
        'entity_type',
        'entity_id',
        'revision_id',
        'source',
        'translate',
      ]);
      $conditions = $query->orConditionGroup();
      $conditions->condition('etr.entity_type', 'fieldable_panels_pane');
      $conditions->isNull('etr.entity_type');
      $query->condition($conditions);
      $query->addExpression("COALESCE([etr].[language], [fpp].[language])", 'language');
      $query->addField('etr', 'uid', 'etr_uid');
      $query->addField('etr', 'status', 'etr_status');
      $query->addField('etr', 'created', 'etr_created');
      $query->addField('etr', 'changed', 'etr_changed');

      $query->orderBy('etr.revision_id');
      $query->orderBy('etr.language');
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    if ($row->getSourceProperty('etr_created')) {
      $row->setSourceProperty('vid', $row->getSourceProperty('revision_id'));
      $row->setSourceProperty('created', $row->getSourceProperty('etr_created'));
      $row->setSourceProperty('timestamp', $row->getSourceProperty('etr_changed'));
      $row->setSourceProperty('revision_uid', $row->getSourceProperty('etr_uid'));
      $row->setSourceProperty('source_langcode', $row->getSourceProperty('source'));
    }

    $row->setSourceProperty('view_access', unserialize($row->getSourceProperty('view_access')));
    $row->setSourceProperty('edit_access', unserialize($row->getSourceProperty('edit_access')));

    $fpid = $row->getSourceProperty('fpid');
    $vid = $row->getSourceProperty('vid');
    $bundle = $row->getSourceProperty('bundle');

    // If this entity was translated using Entity Translation, we need to get
    // its source language to get the field values in the right language.
    // The translations will be migrated by the d7_node_entity_translation
    // migration.
    $entity_translatable = $this->isEntityTranslatable('fieldable_panels_pane') && (int) $this->variableGet('language_content_type_' . $bundle, 0) === 4;
    $source_language = $this->getEntityTranslationSourceLanguage('fieldable_panels_pane', $fpid);
    $language = $entity_translatable && $source_language ? $source_language : $row->getSourceProperty('language');

    if ($row->getSourceProperty('etr_created')) {
      $language = $row->getSourceProperty('language');
    }

    // Get Field API field values.
    foreach ($this->getFields('fieldable_panels_pane', $bundle) as $field_name => $field) {
      // Ensure we're using the right language if the entity and the field are
      // translatable.
      $field_language = $entity_translatable && $field['translatable'] ? $language : NULL;
      $row->setSourceProperty($field_name, $this->getFieldValues('fieldable_panels_pane', $field_name, $fpid, $vid, $field_language));
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'fpid' => $this->t('Fieldable Panels Pane ID'),
      'bundle' => $this->t('Bundle'),
      'link' => $this->t('Link'),
      'path' => $this->t('Path'),
      'reusable' => $this->t('Reusable'),
      'admin_title' => $this->t('Admin title'),
      'admin_description' => $this->t('Admin description'),
      'category' => $this->t('Category'),
      'view_access' => $this->t('View access'),
      'edit_access' => $this->t('Edit access'),
      'created' => $this->t('Created timestamp'),
      'changed' => $this->t('Changed timestamp'),
      'uuid' => $this->t('UUID'),
      'language' => $this->t('Language'),
      'vid' => $this->t('Revision ID'),
      'timestamp' => $this->t('Revision timestamp'),
      'uid' => $this->t('Revision author'),
      'title' => $this->t('Title'),
      'log' => $this->t('Revision log'),
      'vuuid' => $this->t('Revision UUID'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'fpid' => [
        'type' => 'integer',
        'alias' => 'fpp',
      ],
      'vid' => [
        'type' => 'integer',
        'alias' => 'fppr',
      ],
      'language' => [
        'type' => 'string',
        'alias' => 'fpp',
      ],
    ];
  }

}
