<?php

namespace Drupal\panopoly_migrate\Plugin\migrate\source\d7;

use Drupal\migrate\Annotation\MigrateSource;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 Fieldable Panels Pane type source from database.
 *
 * @MigrateSource(
 *   id = "d7_fieldable_panels_pane_type",
 *   source_module = "node"
 * )
 */
class FieldablePanelsPaneType extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    return $this->select('fieldable_panels_pane_type', 't')->fields('t');
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'name' => $this->t('Machine name of the fieldable panels pane type.'),
      'title' => $this->t('Human name of the fieldable panels pane type.'),
      'description' => $this->t('Description of the fieldable panels pane type.'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'name' => [
        'type' => 'string',
        'alias' => 't',
      ]
    ];
  }

}
