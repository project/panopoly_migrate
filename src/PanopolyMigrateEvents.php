<?php


namespace Drupal\panopoly_migrate;

/**
 * Defines events for the 'panopoly_migrate' module.
 */
final class PanopolyMigrateEvents {

  const MIGRATE_LAYOUT = 'panopoly_migrate.migrate_layout';

  const MIGRATE_PANE_PLACEMENT = 'panopoly_migrate.migrate_pane_placement';

  const MIGRATE_PANE = 'panopoly_migrate.migrate_pane';

  const MIGRATE_FORMATTED_TEXT = 'panopoly_migrate.migrate_formatted_text';

}
