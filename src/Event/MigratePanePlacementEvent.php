<?php


namespace Drupal\panopoly_migrate\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event for migrating the region used by a Panels Pane.
 */
class MigratePanePlacementEvent extends Event {

  /**
   * The panelizer display.
   *
   * @var array
   */
  protected $panelizerDisplay;

  /**
   * The layout ID to migrate to.
   *
   * @var string
   */
  protected $layout;

  /**
   * The original pane data.
   *
   * @var array
   */
  protected $pane;

  /**
   * The region to migrate to.
   *
   * @var string
   */
  protected $region;

  /**
   * Stores if we should skip this pane.
   *
   * @var bool
   */
  protected $skip = FALSE;

  /**
   * Constructs a MigratePanePlacementEvent.
   *
   * @param array $panelizer_display
   *   The Panelizer display.
   * @param string $layout
   *   The layout ID to migrate to.
   * @param array $pane
   *   The original pane data.
   */
  public function __construct(array $panelizer_display, $layout, array $pane) {
    $this->layout = $layout;
    $this->pane = $pane;
    $this->region = $pane['panel'];
  }

  /**
   * Gets the Panelizer display.
   *
   * @return array
   *   The Panelizer display.
   */
  public function getPanelizerDisplay(): array {
    return $this->panelizerDisplay;
  }

  /**
   * Gets the original layout ID.
   *
   * @return string
   *   The original layout ID.
   */
  public function getOriginalLayout(): string {
    return $this->panelizerDisplay['layout'];
  }

  /**
   * Gets the original region.
   *
   * @return string
   *   The original region.
   */
  public function getOriginalRegion(): string {
    return $this->pane['panel'];
  }

  /**
   * Gets the layout ID to migrate to.
   *
   * @return string
   *   The layout ID to migrate to.
   */
  public function getLayout(): string {
    return $this->layout;
  }

  /**
   * Gets the original pane data.
   *
   * @return array
   *   The original pane data.
   */
  public function getPane(): array {
    return $this->pane;
  }

  /**
   * Gets the region to migrate to.
   *
   * @return string
   *   The region to migrate to.
   */
  public function getRegion(): string {
    return $this->region;
  }

  /**
   * Sets the region to migrate to.
   *
   * @param string $region
   *   The region to migrate to.
   */
  public function setRegion(string $region): void {
    $this->region = $region;
  }

  /**
   * Checks if this pane should be skipped.
   *
   * @return bool
   *   TRUE if this pane should be skipped; otherwise FALSE.
   */
  public function isSkip(): bool {
    return $this->skip;
  }

  /**
   * Sets if this pane should be skipped.
   *
   * @param bool $skip
   *   TRUE if this pane should be skipped; otherwise FALSE.
   */
  public function setSkip(bool $skip): void {
    $this->skip = $skip;
  }

}
