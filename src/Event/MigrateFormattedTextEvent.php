<?php

namespace Drupal\panopoly_migrate\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event for migrating formatted text.
 */
class MigrateFormattedTextEvent extends Event {

  /**
   * The original full text.
   *
   * @var string
   */
  protected $originalValue;

  /**
   * The original summary.
   *
   * @var string
   */
  protected $originalSummary;

  /**
   * The original text format.
   *
   * @var string
   */
  protected $originalFormat;

  /**
   * The full text.
   *
   * @var string
   */
  protected $value;

  /**
   * The summary.
   *
   * @var string
   */
  protected $summary;

  /**
   * The text format.
   *
   * @var string
   */
  protected $format;

  /**
   * Constructs a new MigrateFormattedTextEvent.
   *
   * @param string $value
   *   The value.
   * @param string $summary
   *   The summary.
   * @param string $format
   *   The format.
   */
  public function __construct($value, $summary, $format) {
    $this->originalValue = $this->value = $value;
    $this->originalSummary = $this->summary = $summary;
    $this->originalFormat = $this->format = $format;
  }

  /**
   * Get the original full text.
   *
   * @return string
   *   The original full text.
   */
  public function getOriginalValue() {
    return $this->value;
  }

  /**
   * Get the original summary.
   *
   * @return string
   *   The original summary.
   */
  public function getOriginalSummary() {
    return $this->summary;
  }

  /**
   * Get the original text format.
   *
   * @return string
   *   The original text format.
   */
  public function getOriginalFormat() {
    return $this->format;
  }

  /**
   * Get the full text.
   *
   * @return string
   *   The full text.
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * Set the full text.
   *
   * @param string $value
   *   The full text.
   */
  public function setValue(string $value) {
    $this->value = $value;
  }

  /**
   * Get the summary.
   *
   * @return string
   *   The summary.
   */
  public function getSummary() {
    return $this->summary;
  }

  /**
   * Set the summary.
   *
   * @param string $summary
   *   The summary.
   */
  public function setSummary(string $summary) {
    $this->summary = $summary;
  }

  /**
   * Get the text format.
   *
   * @return string
   *   The text format.
   */
  public function getFormat() {
    return $this->format;
  }

  /**
   * Set the text format.
   *
   * @param string $format
   *   The text format.
   */
  public function setFormat(string $format) {
    $this->format = $format;
  }

}
