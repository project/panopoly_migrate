<?php

namespace Drupal\panopoly_migrate\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event for migrating Panels Panes.
 */
class MigratePaneEvent extends Event {

  /**
   * The panelizer display.
   *
   * @var array
   */
  protected $panelizerDisplay;

  /**
   * The new layout.
   *
   * @var string
   */
  protected $layout;

  /**
   * The new region.
   *
   * @var string
   */
  protected $region;

  /**
   * The original pane data.
   *
   * @var array
   */
  protected $pane;

  /**
   * The block ID to migrate to.
   *
   * @var string|null
   */
  protected $blockId = NULL;

  /**
   * The block configuration to migrate to.
   *
   * @var array
   */
  protected $blockConfiguration = [];

  /**
   * The block's third party settings.
   *
   * @var array
   */
  protected $thirdPartySettings = [];

  /**
   * The block's weight.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * Stores if we should skip this pane.
   *
   * @var bool
   */
  protected $skip = FALSE;

  /**
   * Constructs a MigratePaneEvent.
   *
   * @param array $panelizer_display
   *   The Panelizer display.
   * @param string $layout
   *   The new layout.
   * @param string $region
   *   The new region.
   * @param array $pane
   *   The original pane data.
   */
  public function __construct(array $panelizer_display, string $layout, string $region, array $pane) {
    $this->panelizerDisplay = $panelizer_display;
    $this->layout = $layout;
    $this->region = $region;
    $this->pane = $pane;
  }

  /**
   * Gets the Panelizer display.
   *
   * @return array
   *   The Panelizer display.
   */
  public function getPanelizerDisplay(): array {
    return $this->panelizerDisplay;
  }

  /**
   * Gets the original layout ID.
   *
   * @return string
   *   The original layout ID.
   */
  public function getOriginalLayout(): string {
    return $this->panelizerDisplay['layout'];
  }

  /**
   * Gets the original region.
   *
   * @return string
   *   The original region.
   */
  public function getOriginalRegion(): string {
    return $this->pane['panel'];
  }

  /**
   * Gets the new layout.
   *
   * @return string
   *   The new layout.
   */
  public function getLayout(): string {
    return $this->layout;
  }

  /**
   * Gets the new region.
   *
   * @return string
   *   The new region.
   */
  public function getRegion(): string {
    return $this->region;
  }

  /**
   * Gets the original pane data.
   *
   * @return array
   *   The original pane data.
   */
  public function getPane(): array {
    return $this->pane;
  }

  /**
   * Gets the block ID to migrate to.
   *
   * @return string|null
   *   The block ID to migrate to.
   */
  public function getBlockId(): ?string {
    return $this->blockId;
  }

  /**
   * Sets the block ID to migrate to.
   *
   * @param string|null $blockId
   *   The block ID to migrate to.
   */
  public function setBlockId(?string $blockId): void {
    $this->blockId = $blockId;
  }

  /**
   * Gets the block configuration to migrate to.
   *
   * @return array
   *   The block configuration to migrate to.
   */
  public function getBlockConfiguration(): array {
    return $this->blockConfiguration;
  }

  /**
   * Sets the block configuration to migrate to.
   *
   * @param array $blockConfiguration
   *   The block configuration to migrate to.
   */
  public function setBlockConfiguration(array $blockConfiguration): void {
    $this->blockConfiguration = $blockConfiguration;
  }

  /**
   * Gets the block's third party settings to migrate to.
   *
   * @return array
   *   The block's third party settings to migrate to.
   */
  public function getThirdPartySettings(): array {
    return $this->thirdPartySettings;
  }

  /**
   * Sets the block's third party settings to migrate to.
   *
   * @param array $thirdPartySettings
   *   The block's third party settings to migrate to.
   */
  public function setThirdPartySettings(array $thirdPartySettings): void {
    $this->thirdPartySettings = $thirdPartySettings;
  }

  /**
   * Gets the block's weight.
   *
   * @return int
   *   The block's weight.
   */
  public function getWeight(): int {
    return $this->weight;
  }

  /**
   * Sets the block's weight.
   *
   * @param int $weight
   *   The block's weight.
   */
  public function setWeight($weight): void {
    $this->weight = $weight;
  }

  /**
   * Checks if this pane should be skipped.
   *
   * @return bool
   *   TRUE if this pane should be skipped; otherwise FALSE.
   */
  public function isSkip(): bool {
    return $this->skip;
  }

  /**
   * Sets if this pane should be skipped.
   *
   * @param bool $skip
   *   TRUE if this pane should be skipped; otherwise FALSE.
   */
  public function setSkip(bool $skip): void {
    $this->skip = $skip;
  }

}
