<?php

namespace Drupal\panopoly_migrate\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event for migrating a Panels layout.
 */
class MigrateLayoutEvent extends Event {

  /**
   * The panelizer display.
   *
   * @var array
   */
  protected $panelizerDisplay;

  /**
   * The original layout ID.
   *
   * @var string
   */
  protected $originalLayout;

  /**
   * The original layout settings.
   *
   * @var array
   */
  protected $originalSettings;

  /**
   * The layout ID to migrate to.
   *
   * @var string
   */
  protected $layout;

  /**
   * The layout settings to migrate to.
   *
   * @var array
   */
  protected $settings;

  /**
   * Constructs a MigrateLayoutEvent.
   *
   * @param array $panelizer_display
   *   The Panelizer display.
   * @param string $layout
   *   The layout ID.
   * @param array $settings
   *   The layout settings.
   */
  public function __construct(array $panelizer_display, $layout, array $settings) {
    $this->panelizerDisplay = $panelizer_display;
    $this->layout = $this->originalLayout = $layout;
    $this->settings = $this->originalSettings = $settings;
  }

  /**
   * Gets the Panelizer display.
   *
   * @return array
   *   The Panelizer display.
   */
  public function getPanelizerDisplay(): array {
    return $this->panelizerDisplay;
  }

  /**
   * Gets the original layout ID.
   *
   * @return string
   *   The original layout ID.
   */
  public function getOriginalLayout(): string {
    return $this->originalLayout;
  }

  /**
   * Gets the original layout settings.
   *
   * @return array
   *   The original layout settings.
   */
  public function getOriginalSettings(): array {
    return $this->originalSettings;
  }

  /**
   * Gets the layout ID to migrate to.
   *
   * @return string
   *   The layout ID to migrate to.
   */
  public function getLayout(): string {
    return $this->layout;
  }

  /**
   * Sets the layout ID to migrate to.
   *
   * @param string $layout
   *   The layout ID to migrate to.
   */
  public function setLayout(string $layout): void {
    $this->layout = $layout;
  }

  /**
   * Gets the layout settings to migrate to.
   *
   * @return array
   *   The layout settings to migrate to.
   */
  public function getSettings(): array {
    return $this->settings;
  }

  /**
   * Sets the layout settings to migrate to.
   *
   * @param array $settings
   *   The layout settings to migrate to.
   */
  public function setSettings(array $settings): void {
    $this->settings = $settings;
  }

}
