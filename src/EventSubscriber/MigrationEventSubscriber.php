<?php

namespace Drupal\panopoly_migrate\EventSubscriber;

use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Drupal\panopoly_migrate\InlineBlockEntityOperations;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to migration events.
 */
class MigrationEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[MigrateEvents::PRE_ROW_SAVE][] = ['onPreRowSave'];
    $events[MigrateEvents::POST_ROW_SAVE][] = ['onPostRowSave'];
    return $events;
  }

  /**
   * Handles MigrateEvents::PRE_ROW_SAVE events.
   *
   * @param \Drupal\migrate\Event\MigratePreRowSaveEvent $event
   *   The event.
   */
  public function onPreRowSave(MigratePreRowSaveEvent $event) {
    InlineBlockEntityOperations::setDisableNormalOperations(TRUE);
  }

  /**
   * Handles MigrateEvents::POST_ROW_SAVE events.
   *
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   *   The event.
   */
  public function onPostRowSave(MigratePostRowSaveEvent $event) {
    InlineBlockEntityOperations::setDisableNormalOperations(FALSE);
  }

}
