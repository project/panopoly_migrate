<?php

namespace Drupal\panopoly_migrate\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\migrate\MigrateLookupInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\panopoly_migrate\Event\MigratePaneEvent;
use Drupal\panopoly_migrate\PanopolyMigrateEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for migrating Fieldable Panels Panes.
 */
class MigrateFieldablePanelsPanesEventSubscriber implements EventSubscriberInterface {

  /**
   * The block content storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $blockContentStorage;

  /**
   * The migrate lookup service.
   *
   * @var \Drupal\migrate\MigrateLookupInterface
   */
  protected $migrateLookup;

  /**
   * The migration manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationManager;

  /**
   * The key value service.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValue;

  /**
   * Migration ids for all FPP migrations.
   *
   * @var string[]|NULL
   */
  protected $fppMigrationIds;

  /**
   * Constructs a MigrateFieldablePanelsPanesEventSubscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\migrate\MigrateLookupInterface $migrate_lookup
   *   The migrate lookup service.
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration_manager
   *   The migration manager.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value
   *   The key value factory.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MigrateLookupInterface $migrate_lookup, MigrationPluginManagerInterface $migration_manager, KeyValueFactoryInterface $key_value) {
    $this->blockContentStorage = $entity_type_manager->getStorage('block_content');
    $this->migrateLookup = $migrate_lookup;
    $this->migrationManager = $migration_manager;
    $this->keyValue = $key_value;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    $events[PanopolyMigrateEvents::MIGRATE_PANE][] = ['onMigratePane'];
    return $events;
  }

  /**
   * Migrates fieldable panels panes.
   *
   * @param \Drupal\panopoly_migrate\Event\MigratePaneEvent $event
   *   The event.
   */
  public function onMigratePane(MigratePaneEvent $event) {
    $pane = $event->getPane();
    $config = [];

    // We only handle FPPs in this event subscriber.
    if ($pane['type'] !== 'fieldable_panels_pane') {
      return;
    }

    if (empty($pane['fpid'])) {
      return;
    }

    $block_content = $this->lookupBlockContent($pane);
    if ($block_content === NULL) {
      return;
    }

    if (!empty($pane['fpp_title'])) {
      $config['label'] = $pane['fpp_title'];
      $config['label_display'] = 'visible';
    }
    else {
      $config['label'] = $block_content->label();
      if (empty($config['label'])) {
        $block_content->getEntityType()->getLabel();
      }
    }

    $config['view_mode'] = 'full';

    if ($pane['reusable']) {
      $event->setBlockId("block_content:" . $block_content->uuid());
      $config['provider'] = 'block_content';
      $config['info'] = '';
      $config['status'] = TRUE;
    }
    else {
      $event->setBlockId("inline_block:" . $block_content->bundle());
      $config['provider'] = 'layout_builder';
      $config['block_revision_id'] = $block_content->getRevisionId();
      $config['block_serialized'] = serialize($block_content);
    }

    $event->setBlockConfiguration($config);
  }

  /**
   * Looks up the block content entity from the pane data.
   *
   * @param array $pane
   *   Associative array with pane data.
   *
   * @return \Drupal\block_content\Entity\BlockContent|NULL
   */
  protected function lookupBlockContent(array $pane) {
    if ($this->fppMigrationIds === NULL) {
      $migrate_last_imported_store = $this->keyValue->get('migrate_last_imported');

      $definitions = array_filter($this->migrationManager->getDefinitions(), function ($definition) use ($migrate_last_imported_store) {
        if ($definition['source']['plugin'] === 'd7_fieldable_panels_pane' && $definition['destination']['plugin'] === 'entity_complete:block_content') {
          // Only return those that have ever been run before.
          return $migrate_last_imported_store->get($definition['id'], FALSE) !== FALSE;
        }
        return FALSE;
      });
      $this->fppMigrationIds = array_keys($definitions);
    }

    try {
      $block_id = $this->migrateLookup->lookup($this->fppMigrationIds, [
        'fpid' => $pane['fpid'],
        'vid' => $pane['vid'],
        'language' => $pane['language'],
      ]);
    }
    catch (\Exception $e) {
      // Error looking up the block id.
      return NULL;
    }
    if (empty($block_id)) {
      // No block id found for those ids.
      return NULL;
    }

    return $this->blockContentStorage->loadRevision($block_id[0]['revision_id']);
  }

}
