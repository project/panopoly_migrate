<?php

namespace Drupal\panopoly_migrate\EventSubscriber;

use Drupal\Core\Database\Database;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Site\Settings;
use Drupal\migrate\MigrateLookupInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\panopoly_migrate\Event\MigratePaneEvent;
use Drupal\panopoly_migrate\PanopolyMigrateEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for migrating default panes.
 */
class MigrateDefaultPanesEventSubscriber implements EventSubscriberInterface {

  const VIEWS_PANES_CONFIGURATION = [
    'view_mode',
    'items_per_page',
    'view_settings',
    'header_type',
    'use_pager',
  ];

  /**
   * The migrate lookup service.
   *
   * @var \Drupal\migrate\MigrateLookupInterface
   */
  protected $migrateLookup;

  /**
   * The migration manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationManager;

  /**
   * The key value service.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * The site settings.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * Migration ids for all node migrations.
   *
   * @var array|null
   */
  protected $nodeMigrationIds;

  /**
   * Constructs a MigrateFieldablePanelsPanesEventSubscriber.
   *
   * @param \Drupal\migrate\MigrateLookupInterface $migrate_lookup
   *   The migrate lookup service.
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration_manager
   *   The migration manager.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value
   *   The key value factory.
   * @param \Drupal\Core\Site\Settings $settings
   *   The site settings.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(MigrateLookupInterface $migrate_lookup, MigrationPluginManagerInterface $migration_manager, KeyValueFactoryInterface $key_value, Settings $settings) {
    $this->migrateLookup = $migrate_lookup;
    $this->migrationManager = $migration_manager;
    $this->keyValue = $key_value;
    $this->settings = $settings;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    // Set priority to -100 so it runs after other event subscribers.
    $events[PanopolyMigrateEvents::MIGRATE_PANE][] = ['onMigratePane', -100];
    return $events;
  }

  /**
   * Responds to the PanopolyMigrateEvents::MIGRATE_PANE event.
   *
   * @param \Drupal\panopoly_migrate\Event\MigratePaneEvent $event
   *   The event.
   */
  public function onMigratePane(MigratePaneEvent $event) {
    $pane = $event->getPane();

    // If an earlier event subscriber already set the block ID, don't change it.
    if (empty($event->getBlockId())) {
      $config = $this->migratePane($pane);
      if (!empty($config['id'])) {
        $event->setBlockId($config['id']);
        unset($config['id']);
        $event->setBlockConfiguration($config);
      }
      elseif ($this->settings::get('panopoly_migrate_skip_unmigrated_panes', FALSE)) {
        $event->setSkip(TRUE);
      }
    }

    $widget_title = NULL;
    if (!empty($pane['configuration']['widget_title'])) {
      // Is this only Views?
      $widget_title = $pane['configuration']['widget_title'];
    }
    if (!empty($pane['configuration']['override_title'])) {
      $widget_title = $pane['configuration']['override_title_text'];
    }
    $config = $event->getBlockConfiguration();
    if ($widget_title) {
      $block_id_parts = explode(':', $event->getBlockId());
      if ($block_id_parts[0] === 'views_block') {
        $config['label'] = '';
        $config['views_label'] = $widget_title;
      }
      else {
        $config['label'] = $widget_title;
      }
      $config['label_display'] = 'visible';
    }
    elseif (empty($config['label'])) {
      $config['label'] = '';
      $config['label_display'] = 0;
    }
    $event->setBlockConfiguration($config);
  }

  /**
   * Generically migrate a Views pane.
   *
   * @param array $pane
   *   The pane.
   * @param array $field_rename
   *   A map between old and new field names.
   * @param array $filter_rename
   *   A map between old and new filter names.
   *
   * @return array
   *   The resulting block configuration.
   */
  public static function migrateViewsPane(array $pane, array $field_rename = [], array $filter_rename = []): array {
    $config = [];

    // Attempt to migrate to a view with the same name.
    $config['id'] = 'views_block:' . $pane['subtype'];

    // Copy top-level values that we know transfer directly.
    foreach (static::VIEWS_PANES_CONFIGURATION as $key) {
      if (isset($pane['configuration'][$key])) {
        $config[$key] = $pane['configuration'][$key];
      }
    }

    // Exposed filters and sorts.
    if (!empty($pane['configuration']['exposed'])) {
      foreach ($pane['configuration']['exposed'] as $key => $value) {
        if (in_array($key, ['sort_order', 'sort_by'])) {
          if ($key === 'sort_by') {
            if (isset($field_rename[$value])) {
              $value = $field_rename[$value];
            }
            if ($value === NULL) {
              continue;
            }
          }
          $config['exposed_form']['sort'][$key] = $value;
        }
        else {
          if (isset($filter_rename[$key])) {
            $key = $filter_rename[$key];
          }
          if ($key === NULL) {
            continue;
          }
          $config['exposed_form']['filters'][$key] = $value;
        }
      }
    }

    // Field overrides.
    if (!empty($pane['fields_override'])) {
      foreach ($pane['configuration']['fields_override'] as $key => $value) {
        if (isset($field_rename[$key])) {
          $key = $field_rename[$key];
        }
        if ($key === NULL) {
          continue;
        }
        $config['fields'][$key]['hide'] = !$value;
      }
    }

    return $config;
  }

  /**
   * Migrates a pane.
   *
   * @param array $pane
   *   The pane.
   *
   * @return array
   *   The resulting block configuration.
   */
  public function migratePane(array $pane): array {
    $config = [];

    if ($pane['type'] === 'views_panes') {
      // Specific Panopoly-provided Views.
      if ($pane['subtype'] === 'panopoly_widgets_general_content-piece_of_content') {
        // Clear config since the new block isn't a View.
        $config['id'] = 'panopoly_widgets_content_item';
        $config['view_mode'] = $pane['configuration']['view_mode'] ?? 'default';
        $config['nid'] = $this->lookupNode($pane['configuration']['exposed']['nid'])
          ?? $pane['configuration']['exposed']['nid'];
      }
      elseif ($pane['subtype'] === 'panopoly_widgets_general_content-list_of_content') {
        $config = static::migrateViewsPane($pane, [
          'field_featured_image' => 'field_panopoly_featured_image',
        ]);
        $config['id'] = 'views_block:panopoly_widgets-list_of_content';
      }
    }
    elseif ($pane['type'] === 'menu_tree') {
      // @todo How can we map from the old menu name to the new one?
      $menu_name = $pane['configuration']['menu_name'];
      if ($menu_name === 'main-menu') {
        $menu_name = 'main';
      }

      $config['id'] = 'system_menu_block:' . $menu_name;
      $config['level'] = $pane['configuration']['level'];
      $config['depth'] = $pane['configuration']['depth'];
      $config['expand_all_items'] = $pane['configuration']['expanded'];
      // @todo Do all the extra menu_block configuration!
    }

    return $config;
  }

  /**
   * Looks up the block content entity from the pane data.
   *
   * @param int $original_nid
   *   The original nid.
   *
   * @return int|null
   *   The current node id.
   */
  protected function lookupNode($original_nid) {
    if ($this->nodeMigrationIds === NULL) {
      $migrate_last_imported_store = $this->keyValue->get('migrate_last_imported');

      $definitions = array_filter($this->migrationManager->getDefinitions(), function ($definition) use ($migrate_last_imported_store) {
        if (in_array($definition['destination']['plugin'], ['entity_complete:node', 'entity:node'])) {
          // Only return those that have ever been run before.
          return $migrate_last_imported_store->get($definition['id'], FALSE) !== FALSE;
        }
        return FALSE;
      });
      $this->nodeMigrationIds = array_keys($definitions);
    }

    try {
      $migrate_db = Database::getConnection('default', 'migrate');
    }
    catch (\Exception $e) {
      return NULL;
    }

    $source_key = $migrate_db->select('node', 'n')
      ->fields('n', ['nid', 'vid', 'language'])
      ->condition('n.nid', $original_nid)
      ->execute()
      ->fetchAssoc();

    try {
      $destination_key = $this->migrateLookup->lookup($this->nodeMigrationIds, $source_key);
    }
    catch (\Exception $e) {
      // Error looking up the node id.
      return NULL;
    }
    if (empty($destination_key)) {
      // No node id found for those ids.
      return NULL;
    }

    return $destination_key[0]['nid'];
  }

}
