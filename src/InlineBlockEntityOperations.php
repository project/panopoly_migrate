<?php

namespace Drupal\panopoly_migrate;

use Drupal\Core\Entity\EntityInterface;
use Drupal\layout_builder\InlineBlockEntityOperations as UpstreamInlineBlockEntityOperations;
use Drupal\layout_builder\SectionComponent;

/**
 * Replaces InlineBlockEntityOperations when running a migration.
 */
class InlineBlockEntityOperations extends UpstreamInlineBlockEntityOperations {

  /**
   * Whether or not to disable normal operations.
   *
   * @var bool
   */
  static protected $disableNormalOperations = FALSE;

  /**
   * Checks if normal operations are being disabled.
   *
   * @return bool
   */
  public static function isDisableNormalOperations(): bool {
    return static::$disableNormalOperations;
  }

  /**
   * Enables or disables normal operations.
   *
   * @param bool $disableNormalOperations
   */
  public static function setDisableNormalOperations(bool $disableNormalOperations): void {
    static::$disableNormalOperations = $disableNormalOperations;
  }

  /**
   * {@inheritDoc}
   */
  protected function saveInlineBlockComponent(EntityInterface $entity, SectionComponent $component, $new_revision, $duplicate_blocks) {
    if (static::$disableNormalOperations === FALSE) {
      parent::saveInlineBlockComponent($entity, $component, $new_revision, $duplicate_blocks);
      return;
    }

    // We don't ever duplicate or create a new revision, but we do update the
    // usage info always.

    /** @var \Drupal\layout_builder\Plugin\Block\InlineBlock $plugin */
    $plugin = $component->getPlugin();
    $plugin->saveBlockContent();
    $configuration = $plugin->getConfiguration();

    if (!empty($configuration['block_revision_id'])) {
      $block_content = $this->blockContentStorage->loadRevision($configuration['block_revision_id']);
      if ($block_content) {
        // Since we are importing all revisions, the same block can have it's
        // usage added multiple times, leading to an error if the entry is
        // already in the database.
        $this->usage->deleteUsage([ $block_content->id() ]);
        $this->usage->addUsage($block_content->id(), $entity);
      }
    }

    $component->setConfiguration($configuration);
  }

}