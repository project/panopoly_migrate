<?php

namespace Drupal\panopoly_migrate;

use Drupal\Core\DependencyInjection\ClassResolver;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\layout_builder\InlineBlockEntityOperations;
use Drupal\panopoly_migrate\InlineBlockEntityOperations as PanopolyMigrateInlineBlockEntityOperations;

/**
 * Class resolver to allow us to replace InlineBlockEntityOperations.
 */
class ClassResolverDecorator extends ClassResolver {

  /**
   * The original class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * ClassResolverDecorator constructor.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The original class resolver.
   */
  public function __construct(ClassResolverInterface $class_resolver) {
    $this->classResolver = $class_resolver;
  }

  /**
   * {@inheritDoc}
   */
  public function getInstanceFromDefinition($definition) {
    if ($definition === InlineBlockEntityOperations::class) {
      return $this->classResolver->getInstanceFromDefinition(PanopolyMigrateInlineBlockEntityOperations::class);
    }

    return $this->classResolver->getInstanceFromDefinition($definition);
  }

}