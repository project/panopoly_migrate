<?php

namespace Drupal\panopoly_migrate_wysiwyg\EventSubscriber;

use Drupal\Core\Site\Settings;
use Drupal\panopoly_migrate\Event\MigrateFormattedTextEvent;
use Drupal\panopoly_migrate\PanopolyMigrateEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Migrates formatted text from Panopoly WYSIWYG.
 */
class MigrateWysiwygFormattedTextEventSubscriber implements EventSubscriberInterface {

  /**
   * The text formats from Panopoly 1.x.
   */
  const PANOPOLY_V1_FORMATS = [
    'panopoly_wysiwyg_text',
    'panopoly_html_text',
  ];

  /**
   * The site settings service.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * Constructs a MigrateWysiwygFormattedTextEventSubscriber.
   *
   * @param \Drupal\Core\Site\Settings $settings
   *   The site settings service.
   */
  public function __construct(Settings $settings) {
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[PanopolyMigrateEvents::MIGRATE_FORMATTED_TEXT][] = [
      'onMigrateFormattedText',
      // Run after user defined event subscribers.
      -100,
    ];
    return $events;
  }

  /**
   * Migrates formatted text.
   *
   * @param Drupal\panopoly_migrate\Event\MigrateFormattedTextEvent $event
   *   The event.
   */
  public function onMigrateFormattedText(MigrateFormattedTextEvent $event) {
    $formats = static::PANOPOLY_V1_FORMATS;
    if (!$this->settings->get('panopoly_migrate_keep_full_html', FALSE)) {
      $formats[] = 'full_html';
    }
    if (in_array($event->getFormat(), $formats)) {
      $event->setFormat('panopoly_wysiwyg_basic');
    }
  }

}
