<?php

namespace Drupal\panopoly_migrate_widgets\Plugin\migrate\source\d7;

use Drupal\migrate\Annotation\MigrateSource;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Panopoly 1.x spotlight item source plugin.
 *
 * @MigrateSource(
 *   id = "d7_panopoly_spotlight_item",
 *   source_module = "panopoly_widgets"
 * )
 */
class SpotlightItem extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $field_name = $this->configuration['field_name'];
    $entity_type = $this->configuration['entity_type'];

    $query = $this->select('field_revision_' . $field_name, 'f')
      ->fields('f', [
        'entity_type',
        'bundle',
        'entity_id',
        'revision_id',
        'language',
        'delta',
      ])
      ->condition('f.deleted', 0)
      ->orderBy('f.entity_type')
      ->orderBy('f.revision_id')
      ->orderBy('f.delta');

    $query->addField('f', "{$field_name}_title", 'title');
    $query->addField('f', "{$field_name}_description", 'description');
    $query->addField('f', "{$field_name}_fid", 'fid');
    $query->addField('f', "{$field_name}_link", 'link');
    $query->addField('f', "{$field_name}_alt", 'alt');

    if ($entity_type === 'fieldable_panels_pane') {
      $query->condition('f.entity_type', $entity_type);

      $query->join('fieldable_panels_panes_revision', 'fppr', 'f.revision_id = fppr.vid');
      $query->fields('fppr', [
        'fpid',
        'timestamp',
        'uid',
      ]);
    }

    return $query;
  }

  public function prepareRow(Row $row) {
    // Make a convenient 'fpp_key' property.
    if ($row->hasSourceProperty('fpid')) {
      $row->setSourceProperty('fpp_key', [
        'fpid' => $row->getSourceProperty('fpid'),
        'vid' => $row->getSourceProperty('revision_id'),
        'language' => $row->getSourceProperty('language'),
      ]);
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'entity_type' => $this->t('Machine name of the entity type.'),
      'bundle' => $this->t('Bundle'),
      'entity_id' => $this->t('Entity ID'),
      'revision_id' => $this->t('Revision ID'),
      'language' => $this->t('Language'),
      'delta' => $this->t('Delta'),
      'title' => $this->t('Slide title'),
      'description' => $this->t('Slide description'),
      'fid' => $this->t('Slide image file ID'),
      'link' => $this->t('Slide link'),
      'alt' => $this->t('Slide alt text'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'entity_type' => [
        'type' => 'string',
        'alias' => 'f',
      ],
      'revision_id' => [
        'type' => 'integer',
        'alias' => 'f',
      ],
      'delta' => [
        'type' => 'integer',
        'alias' => 'f',
      ],
    ];
  }

}
