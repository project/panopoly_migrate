<?php

namespace Drupal\panopoly_migrate_widgets\Plugin\migrate\process;

use Drupal\migrate\Annotation\MigrateProcessPlugin;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Converts the table field from D7.
 *
 * @MigrateProcessPlugin(
 *   id = "panopoly_migrate_widgets_table",
 *   handle_multiples = TRUE
 * )
 */
class Table extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $tables = [];

    foreach ($value as $data) {
      $original = unserialize($data['value']);

      $table = [];
      foreach ($original['tabledata'] as $original_row) {
        $table_row = [];
        foreach ($original_row as $col_name => $original_col) {
          if (strpos($col_name, 'col_') === 0) {
            $table_row[] = $original_col;
          }
        }
        $table[] = $table_row;
      }
      $table["caption"] = $original['caption'];

      $tables[] = [
        'caption' => $original['caption'],
        'value' => $table,
      ];
    }

    return $tables;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return TRUE;
  }

}

