<?php

namespace Drupal\panopoly_migrate_widgets\Plugin\migrate\process;

use Drupal\migrate\Annotation\MigrateProcessPlugin;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Converts the table field from D7.
 *
 * @MigrateProcessPlugin(
 *   id = "panopoly_migrate_spotlight_field",
 *   handle_multiples = TRUE
 * )
 */
class SpotlightField extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $paragraphs = [];

    foreach ($value as $item) {
      $paragraphs[] = $this->createParagraphItem($item);
    }

    return $paragraphs;
  }

  protected function createParagraphItem(array $value) {
    $paragraph = Paragraph::create([
      'type' => 'panopoly_spotlight',
      'field_panopoly_spotlight_body' => [
        'value' => $value['description'],
      ],
      'field_panopoly_spotlight_title' => [
        'value' => $value['title'],
      ],
      'field_panopoly_spotlight_link' => [
        'uri' => $value['link'],
      ],
      'field_panopoly_spotlight_image' => [
        'target_id' => $value['media'],
      ],
    ]);

    $paragraph->save();

    return [
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return TRUE;
  }

}
