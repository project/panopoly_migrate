<?php

namespace Drupal\panopoly_migrate_media;

/**
 * Defines events for the 'panopoly_migrate_media' module.
 */
final class PanopolyMigrateMediaEvents {

  const MIGRATE_MEDIA_TAG = 'panopoly_migrate_media.migrate_media_tag';

}
