<?php

namespace Drupal\panopoly_migrate_media\EventSubscriber;

use Drupal\panopoly_migrate_media\Event\MigrateMediaTagEvent;
use Drupal\panopoly_migrate_media\PanopolyMigrateMediaEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for migrating default panes.
 */
class MigrateMediaTagEventSubscriber implements EventSubscriberInterface {

  const VIEW_MODE_MAP = [
    'default' => 'view_mode:media.embed_large',
    'teaser' => 'view_mode:media.embed_medium',
    'preview' => 'view_mode:media.embed_small',
  ];

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[PanopolyMigrateMediaEvents::MIGRATE_MEDIA_TAG][] = ['onMigrateMediaTag', 0];
    return $events;
  }

  /**
   * Migrates from media tag attributes to embed attributes.
   *
   * @param Drupal\panopoly_migrate_media\Event\MigrateMediaTagEvent $event
   *   The migrate media tag event.
   */
  public function onMigrateMediaTag(MigrateMediaTagEvent $event) {
    $media_tag = $event->getMediaTag();
    $embed = $event->getEmbedAttributes();

    // The embed button.
    if (empty($embed['data-embed-button'])) {
      $embed['data-embed-button'] = 'panopoly_media_wysiwyg_media_embed';
    }

    // Convert view mode.
    if (empty($embed['data-entity-embed-display'])) {
      $view_mode = $media_tag['view_mode'] ?? 'default';
      if (!isset(static::VIEW_MODE_MAP[$view_mode])) {
        $view_mode = 'default';
      }
      $embed['data-entity-embed-display'] = static::VIEW_MODE_MAP[$view_mode];
    }

    $attributes = $media_tag['attributes'] ?? [];

    // Add alt and title overrides.
    foreach (['alt', 'title'] as $attribute) {
      if (empty($embed[$attribute]) && !empty($attributes[$attribute])) {
        $embed[$attribute] = $attributes[$attribute];
      }
    }

    // Convert style attribute to image alignment.
    if (!empty($attributes['style'])) {
      $style = $this->parseStyle($attributes['style']);
      if (isset($style['margin-left']) && $style['margin-left'] === 'auto' && isset($style['margin-right']) && $style['margin-right'] === 'auto') {
        $embed['data-align'] = 'center';
      }
      elseif (isset($style['float'])) {
        if (in_array($style['float'], ['left', 'right'])) {
          $embed['data-align'] = $style['float'];
        }
      }
    }

    $event->setEmbedAttributes($embed);
  }

  /**
   * Parse a style attribute into an array of properties.
   *
   * @param string $style
   *   The style attribute.
   *
   * @return array
   *   An associative array, keyed by the property name, and containing the
   *   property values.
   */
  protected function parseStyle($style) {
    $properties = [];
    foreach (explode(";", $style) as $property) {
      $pair = array_map('trim', explode(":", $property, 2));
      if (count($pair) == 2) {
        $properties[$pair[0]] = $pair[1];
      }
    }
    return $properties;
  }

}
