<?php

namespace Drupal\panopoly_migrate_media\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Template\Attribute;
use Drupal\migrate\MigrateException;
use Drupal\panopoly_migrate\Event\MigrateFormattedTextEvent;
use Drupal\panopoly_migrate\PanopolyMigrateEvents;
use Drupal\panopoly_migrate_media\Event\MigrateMediaTagEvent;
use Drupal\panopoly_migrate_media\PanopolyMigrateMediaEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Migrates formatted text from Panopoly WYSIWYG.
 */
class MigrateMediaFormattedTextEventSubscriber implements EventSubscriberInterface {

  const MEDIA_TAG_REGEX = '/\[\[\s*(\{.+\})\s*\]\]/sU';

  /**
   * The entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a MigrateMediaFormattedTextEventSubscriber.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[PanopolyMigrateEvents::MIGRATE_FORMATTED_TEXT][] = [
      'onMigrateFormattedText',
      0,
    ];
    return $events;
  }

  /**
   * Migrates formatted text.
   *
   * @param Drupal\panopoly_migrate\Event\MigrateFormattedTextEvent $event
   *   The event.
   */
  public function onMigrateFormattedText(MigrateFormattedTextEvent $event) {
    $event->setValue($this->convertMediaTags($event->getValue()));
    $event->setSummary($this->convertMediaTags($event->getSummary()));
  }

  /**
   * Converts media tags.
   *
   * @param string $input
   *   The input text.
   *
   * @return string
   *   The converted text.
   */
  protected function convertMediaTags($input) {
    $output = preg_replace_callback(static::MEDIA_TAG_REGEX, function ($matches) {
      $json = preg_replace('/\s+/', ' ', $matches[1]);
      $data = json_decode($json, TRUE);

      if (!is_array($data) || !isset($data['fid'])) {
        return $matches[0];
      }

      /** @var Drupal\Core\Entity\EntityStorageInterface $media_storage */
      $media_storage = $this->entityTypeManager->getStorage('media');
      /** @var Drupal\media\MediaInterface $media */
      $media = $media_storage->load($data['fid']);

      if ($media === NULL) {
        throw new MigrateException("Unable to load embedded media entity for fid '{$data['fid']}'");
      }

      $embed = [
        'data-entity-type' => 'media',
        'data-entity-uuid' => $media->uuid(),
      ];

      $event = new MigrateMediaTagEvent($data, $embed);
      $this->eventDispatcher->dispatch($event, PanopolyMigrateMediaEvents::MIGRATE_MEDIA_TAG);

      $embed = $event->getEmbedAttributes();
      ksort($embed);

      $attributes = new Attribute($embed);
      return "<drupal-entity {$attributes}></drupal-entity>";
    }, $input);

    return $output;
  }

}
