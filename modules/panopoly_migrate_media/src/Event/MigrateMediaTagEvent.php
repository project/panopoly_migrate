<?php

namespace Drupal\panopoly_migrate_media\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event for migrating media tags.
 */
class MigrateMediaTagEvent extends Event {

  /**
   * The Panopoly 1.x media tag attributes.
   *
   * @var array
   */
  protected $mediaTag;

  /**
   * The Panopoly 2.x entity embed attributes.
   *
   * @var array
   */
  protected $embedAttributes;

  /**
   * Constructs a new MigrateMediaTagEvent.
   *
   * @param array $media_tag
   *   The Panopoly 1.x media tag attibutes.
   * @param array $embed_attributes
   *   The Panopoly 2.x  entity embed attributes.
   */
  public function __construct(array $media_tag, array $embed_attributes = []) {
    $this->mediaTag = $media_tag;
    $this->embedAttributes = $embed_attributes;
  }

  /**
   * Get the Panopoly 1.x media tag attributes.
   *
   * @return array
   *   The Panopoly 1.x media tag attributes.
   */
  public function getMediaTag() {
    return $this->mediaTag;
  }

  /**
   * Get the Panopoly 2.x entity embed attributes.
   *
   * @return array
   *   The Panopoly 2.x entity embed attributes.
   */
  public function getEmbedAttributes() {
    return $this->embedAttributes;
  }

  /**
   * Set the Panopoly 2.x entity embed attributes.
   *
   * @param array $embedAttributes
   *   The Panopoly 2.x entity embed attributes.
   */
  public function setEmbedAttributes(array $embedAttributes) {
    $this->embedAttributes = $embedAttributes;
  }

}
