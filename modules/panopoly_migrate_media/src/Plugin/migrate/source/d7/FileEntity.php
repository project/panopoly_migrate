<?php

namespace Drupal\panopoly_migrate_media\Plugin\migrate\source\d7;

use Drupal\migrate\Annotation\MigrateSource;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;

/**
 * Drupal 7 file entity source from database.
 *
 * @MigrateSource(
 *   id = "panopoly_migrate_file_entity",
 *   source_module = "file_entity"
 * )
 */
class FileEntity extends FieldableEntity {

  public function query() {
    $query = $this->select('file_managed', 'fm')
      ->fields('fm', [
        'fid',
        'uid',
        'filename',
        'uri',
        'filemime',
        'filesize',
        'status',
        'timestamp',
        'type',
        'uuid',
      ])
      ->condition('fm.status', TRUE);

    if (!empty($this->configuration['type'])) {
      $query->condition('fm.type', $this->configuration['type']);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $fid = $row->getSourceProperty('fid');
    $type = $row->getSourceProperty('type');

    // Image height and width are stored in 'file_metadata'.
    $metadata_query = $this->select('file_metadata', 'fmd')
      ->fields('fmd', ['name', 'value'])
      ->condition('fmd.fid', $fid)
      ->execute()->fetchAll(\PDO::FETCH_ASSOC);
    foreach ($metadata_query as $result) {
      $row->setSourceProperty($result['name'], unserialize($result['value']));
    }

    // Get Field API field values.
    foreach ($this->getFields('file', $type) as $field_name => $field) {
      $row->setSourceProperty($field_name, $this->getFieldValues('file', $field_name, $fid));
    }

    return parent::prepareRow($row);
  }

  public function fields() {
    $fields = [
      'fid' => $this->t('File ID'),
      'uid' => $this->t('User ID'),
      'uri' => $this->t('URI'),
      'filemime' => $this->t('File mime'),
      'filesize' => $this->t('File size'),
      'status' => $this->t('Status'),
      'timestamp' => $this->t('Timestamp'),
      'type' => $this->t('Type'),
      'uuid' => $this->t('UUID'),
    ];
    return $fields;
  }

  public function getIds() {
    return [
      'fid' => [
        'type' => 'integer',
        'alias' => 'fm',
      ],
    ];
  }

}