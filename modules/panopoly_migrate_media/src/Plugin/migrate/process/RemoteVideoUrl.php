<?php

namespace Drupal\panopoly_migrate_media\Plugin\migrate\process;

use Drupal\migrate\Annotation\MigrateProcessPlugin;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Converts media URI values to oembed links.
 *
 * @MigrateProcessPlugin(
 *   id = "panopoly_migrate_media_remote_video_url",
 *   handle_multiples = TRUE
 * )
 */
class RemoteVideoUrl extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return preg_replace([
      '/^youtube:\/\/v\//i',
      '/^vimeo:\/\/v\//i',
    ], [
      'https://www.youtube.com/watch?v=',
      'https://vimeo.com/',
    ], $value);
  }

}

