Panopoly Migrate
================

To migrate to Panopoly 2.x (Drupal 8) from Panopoly 1.x (Drupal 7), you must
first install a Panopoly 2.x site from scratch.

** DON'T ENABLE THE DEMO CONTENT! **

Trying to migrate to a Panopoly 2.x site which has the demo content enabled
will break the migration process. Even if it succeeds, you can't disable the
demo content module or it could delete some of your content.

Migrating via the UI
--------------------

@todo Write me!

Migrating via Drush
-------------------

Migrating via Drush allows more flexibility: the process can be run more than
once, and you can customize the migration (although, doing so is quite advanced
and requires a good understanding of the Migrate API in Drupal 8).

 1. Install panopoly_migrate, migrate_upgrade, migrate_plus and migrate_tools

 2. Add the database credentials for your Panopoly 1.x (Drupal 7) site in
    settings.php (or settings.local.php if you use that file):

    $databases['migrate']['default'] = array (
      'database' => 'dbname',
      'username' => 'dbuser',
      'password' => 'dbpass',
      'prefix' => '',
      'host' => 'localhost',
      'port' => '3306',
      'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
      'driver' => 'mysql',
    );

 3. Generate migration configuration:

    drush migrate-upgrade --configure-only --legacy-db-key=migrate --legacy-root=/PATH/TO/OLD/PANOPOLY-7.x/SITE

 4. (optional) Export the configuration so it can be edited:

    drush config-export --destination=/tmp/panopoly-migration

 5. (optional) Make necessary changes to configuration files named like
    migrate_plus.migration.upgrade_*.yml - this can even be stored in Git!

 6. (optional) Re-import the configuration with your changes:

    drush config-import --source=/tmp/panopoly-migration

 7. Run the migration:

    drush migrate-import --group=migrate_drupal_7

 8. (optional) Check status, show messages or rollback:

    drush migrate-status --group=migrate_drupal_7
    drush migrate-messages MIGRATION_NAME
    drush migrate-rollback --group=migrate_drupal_7

